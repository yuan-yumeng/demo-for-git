﻿namespace DemoForGit
{
    internal class Program
    {
        /// <summary>
        /// 移动字符小游戏（文档注释）
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //首先，设置整数x,y，表明初始坐标
            int x = 0, y = 0;
            //声明input,扩大作用域，ConsoleKey是一个枚举类型
            ConsoleKey input;
            //do循环，至少在（0，0）处写Hello！，(do循环会至少执行一次）
            do
            {
                //用Clear清屏
                Console.Clear();
                //然后，在（x,y)输出字符串"Hello!"
                Console.SetCursorPosition(x, y);
                Console.WriteLine("Hello!");
                //最后，通过Console.ReadKey输入
                //这里不需要再声明input, （.Key为属性）
                //ture是为了输入字符来移动Hello!时，所输入的字符不会闪现一下
                input = Console.ReadKey(true).Key;
                //根据使用者输入的内容改变Hello！的坐标
                //用switch循环来检索用户输入的内容
                switch (input)
                {
                    case ConsoleKey.Enter:
                        break;
                    //x轴向右为正，y轴向下为正
                    //用键盘上的W,S,A,D来控制字符Hello!上下左右移动
                    //要是想用键盘上的上下左右键来控制移动，可以写LeftArrow,RightArrow,UpArrow,DownArrow
                    case ConsoleKey.A:
                        if (x > 0)
                            x--;
                        break;
                    case ConsoleKey.D:
                        if (x < 80)
                            x++;
                        break;
                    case ConsoleKey.W:
                        if (y > 0)
                            y--;
                        break;
                    case ConsoleKey.S:
                        if (y < 80)
                            y++;
                        break;
                    default:
                        break;
                }
                //回到第二步
            } while (input != ConsoleKey.Enter);
            
        }
    }
}